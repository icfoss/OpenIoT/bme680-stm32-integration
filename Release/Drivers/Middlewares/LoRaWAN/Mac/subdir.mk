################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Mac/%.o: ../Drivers/Middlewares/LoRaWAN/Mac/%.c Drivers/Middlewares/LoRaWAN/Mac/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -DUSE_HAL_DRIVER -DSTM32L072xx -c -I../Drivers/CMSIS/Include -I../Core/Inc -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc/Legacy -O3 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Mac

clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Mac:
	-$(RM) ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.o ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.d ./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.o

.PHONY: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Mac

