################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Crypto/aes.c \
../Drivers/Middlewares/LoRaWAN/Crypto/cmac.c \
../Drivers/Middlewares/LoRaWAN/Crypto/soft-se.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Crypto/aes.o \
./Drivers/Middlewares/LoRaWAN/Crypto/cmac.o \
./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Crypto/aes.d \
./Drivers/Middlewares/LoRaWAN/Crypto/cmac.d \
./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Crypto/%.o: ../Drivers/Middlewares/LoRaWAN/Crypto/%.c Drivers/Middlewares/LoRaWAN/Crypto/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -DUSE_HAL_DRIVER -DSTM32L072xx -c -I../Drivers/CMSIS/Include -I../Core/Inc -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc/Legacy -O3 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Crypto

clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Crypto:
	-$(RM) ./Drivers/Middlewares/LoRaWAN/Crypto/aes.d ./Drivers/Middlewares/LoRaWAN/Crypto/aes.o ./Drivers/Middlewares/LoRaWAN/Crypto/cmac.d ./Drivers/Middlewares/LoRaWAN/Crypto/cmac.o ./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.d ./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.o

.PHONY: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Crypto

