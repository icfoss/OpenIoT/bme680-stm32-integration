/*
 * battery_read.h
 *
 *  Created on: Jan 29, 2022
 *      Author: jaroos
 */

#ifndef BATTERY_VOLTAGE_BATTERY_READ_H_
#define BATTERY_VOLTAGE_BATTERY_READ_H_



uint16_t readBatteryVoltage(void);
//void readBatteryLevel(battery_v *battery_data);

#endif /* BATTERY_VOLTAGE_BATTERY_READ_H_ */
