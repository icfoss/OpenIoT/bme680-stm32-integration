/*
 * environment_monitoring_app.c
 *
 *  Created on: 08-Feb-2022
 *      Author: jaroos
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "hw.h"
#include "bme680_defs.h"
#include "environment_monitoring_app.h"
#include "i2c.h"
#include "bme680.h"
#include "battery_read.h"



#ifndef BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_C_
#define BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_C_




uint16_t meas_period;
uint16_t Humidity = 0;
uint16_t Temperature = 0;
uint32_t Pressure = 0;
uint32_t gasResistance = 0;
int8_t rslt = BME680_OK;

struct bme680_field_data data;
struct bme680_dev gas_sensor;

void airQualityInit(){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
	MX_I2C1_Init();
	BME680GPIO_Init();
	bme680_init(NULL);

}

void BME680Config(){


	//uint8_t rslt;
	//bme680_self_test(&val);
	// uint16_t p=(0x77<<1);
	// bme680_get_regs(uint8_t reg_addr, uint8_t *reg_data, uint16_t len, struct bme680_dev *dev)

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);



	gas_sensor.dev_id = BME680_I2C_ADDR_SECONDARY;
	gas_sensor.intf = BME680_I2C_INTF;
	gas_sensor.read = user_i2c_read;
	gas_sensor.write = user_i2c_write;
	gas_sensor.delay_ms = user_delay_ms;
	/* amb_temp can be set to 25 prior to configuring the gas sensor
	 * or by performing a few temperature readings without operating the gas sensor.
	 */
	gas_sensor.amb_temp = 25;
	uint16_t p = (0x77 << 1);

	rslt = bme680_init(&gas_sensor);
	//   rslt = bme680_self_test(&gas_sensor);
	//   bme680_get_profile_dur(&meas_period, &gas_sensor);



	if (HAL_I2C_IsDeviceReady(&hi2c1, p, 4, 100) == HAL_OK) {
		printf("ok");
	}
	uint8_t set_required_settings;

	/* Set the temperature, pressure and humidity settings */
	gas_sensor.tph_sett.os_hum = BME680_OS_2X;
	gas_sensor.tph_sett.os_pres = BME680_OS_4X;
	gas_sensor.tph_sett.os_temp = BME680_OS_8X;
	gas_sensor.tph_sett.filter = BME680_FILTER_SIZE_3;

	/* Set the remaining gas sensor settings and link the heating profile */
	gas_sensor.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
	/* Create a ramp heat waveform in 3 steps */
	gas_sensor.gas_sett.heatr_temp = 320; /* degree Celsius */
	gas_sensor.gas_sett.heatr_dur = 150; /* milliseconds */

	/* Select the power mode */
	/* Must be set before writing the sensor configuration */
	gas_sensor.power_mode = BME680_FORCED_MODE;

	/* Set the required sensor settings needed */
	set_required_settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL
			| BME680_FILTER_SEL | BME680_GAS_SENSOR_SEL;

	/* Set the desired sensor configuration */
	rslt = bme680_set_sensor_settings(set_required_settings, &gas_sensor);

	/* Set the power mode */
	rslt = bme680_set_sensor_mode(&gas_sensor);
}

void BME680Read() {

	BME680Config();

	HAL_Delay(1000);
	//HAL_Delay(meas_period); /* Delay till the measurement is ready */

	user_delay_ms(meas_period); /* Delay till the measurement is ready */
    PRINTF("...IN BME READ...\r\n");


	for (int i = 0; i <= 10; i++)

	{
		rslt = bme680_get_sensor_data(&data, &gas_sensor);

		if (i == 10) {
//			printf("T:%.2f degC, P: %.2f hPa, H %.2f H \r\n ",data.temperature, data.pressure,data.humidity);
//
//			/* Avoid using measurements from an unstable heating setup */
			if (data.status & BME680_GASM_VALID_MSK)
//				printf("G: %.2f ohms\r\n", data.gas_resistance);
//
//			printf("\r\n");

			/* Trigger the next measurement if you would like to read data out continuously */
			if (gas_sensor.power_mode == BME680_FORCED_MODE) {
				rslt = bme680_set_sensor_mode(&gas_sensor);
			}


		}
	}
	  rslt = bme680_get_sensor_data(&data, &gas_sensor);
	  Humidity = data.humidity;
	  Temperature = data.temperature;
	  Pressure = data.pressure;
	  gasResistance = data.gas_resistance;
	  PRINTF("Humidity Level = %d \n\r", Humidity);
	  PRINTF("Temperature Level = %d \n\r", Temperature);
	  PRINTF("Pressure Level = %d \n\r", Pressure);
	  PRINTF("gasResistance Level = %d \n\r", gasResistance);


}


void energyHarvesting(energyHarvesting_t* sensor_data) {
	BME680Read();

	sensor_data->batteryLevel = readBatteryVoltage();
	sensor_data->humidity = Humidity;
	sensor_data->temperature = Temperature;
	sensor_data->pressure = Pressure;
	sensor_data->gasResistance = gasResistance;



}

void user_delay_ms(uint32_t period) {
	/*
	 * Return control or wait,
	 * for a period amount of milliseconds
	 */
	HAL_Delay(period);
}

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint16_t Size = 2;
	if (HAL_I2C_Mem_Read(&hi2c1, (dev_id << 1), reg_addr, I2C_MEMADD_SIZE_8BIT,
			reg_data, len, 500) == HAL_OK) {
		rslt = 0;
	} else

		rslt = 1;

	/*
	 * The parameter dev_id can be used as a variable to store the I2C address of the device
	 */

	/*
	 * Data on the bus should be like
	 * |------------+---------------------|
	 * | I2C action | Data                |
	 * |------------+---------------------|
	 * | Start      | -                   |
	 * | Write      | (reg_addr)          |
	 * | Stop       | -                   |
	 * | Start      | -                   |
	 * | Read       | (reg_data[0])       |
	 * | Read       | (....)              |
	 * | Read       | (reg_data[len - 1]) |
	 * | Stop       | -                   |
	 * |------------+---------------------|
	 */

	return rslt;
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	if (HAL_I2C_Mem_Write(&hi2c1, (dev_id << 1), reg_addr, 1, reg_data, len,
			5000) == HAL_OK) {

		rslt = 0;
	} else
		rslt = 1;
	/*
	 * The parameter dev_id can be used as a variable to store the I2C address of the device
	 */

	/*
	 * Data on the bus should be like
	 * |------------+---------------------|
	 * | I2C action | Data                |
	 * |------------+---------------------|
	 * | Start      | -                   |
	 * | Write      | (reg_addr)          |
	 * | Write      | (reg_data[0])       |
	 * | Write      | (....)              |
	 * | Write      | (reg_data[len - 1]) |
	 * | Stop       | -                   |
	 * |------------+---------------------|
	 */

	return rslt;
}

void BME680GPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HW_GPIO_Init(GPIOA, GPIO_PIN_8, &initStruct);

//	HW_GPIO_Init(GPIOB, GPIO_PIN_15, &initStruct);
//	HW_GPIO_Init(GPIOB, GPIO_PIN_2, &initStruct);
//	HW_GPIO_Init(GPIOB, GPIO_PIN_6, &initStruct);
}


#endif /* BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_C_ */
